ino jk <esc> 
cno jk <c-c> 

set tabstop=4 
"apretar el tabulador ahora sera lo mismo que apretar 4 veces el espacio

set softtabstop=4 
"numero de espacios que quitas al apretar backtab

set expandtab 
"convierte los tabs en spaces

set number 
"numera las lineas

set cursorline 
"subralla o destaca la linea en la que estoy 

filetype indent on 
"detecte el tipo de fichero y permite utilizar plugins de indentacion acorde al type



set wildmenu 
"una especie de autocomplete para seleccion de archivos en lso menus de vim

set wildmode=longest:full,full 
"parametros de wildmenu, no se que hacen pero son los mas comunes

set lazyredraw 
"no hace refresh de los files si no es necesario 

set showmatch 
"subraya o destaca directamente los parentesis que se abren o cierran

set incsearch 
"permite hacer busquedas

set hlsearch 
"subraya o destaca los resultados de una busqueda

nnoremap <leader><space> :nohlsearch<CR> 
"cuando escribo ,<space> me permite dejar de subrayar los results

set foldenable 
"permite folding

set foldlevelstart=3 
"hara folding por defecto de los 3 primeros levels de nesting

nnoremap <space> za 
"al apretar espacio me hara un fold o un unfold del codigo

set foldmethod=indent 
"hace el folding segun la indentacion del fichero

set showcmd 
"permite ver que comando estoy escribiendo

nnoremap B ^
"hace que B sea la forma de ir al principio de la linea 

nnoremap W $
"hace que E sea la forma de ir al final de la linea

let mapleader="," 
"\ is a comma 

nnoremap <leader>s :mksession<CR>
" save session cuando escribes ,s se guarda para que la proxima vez que entres en vim haciendo vim -S entre ya con la ultima sesion 

call pathogen#infect()
call pathogen#helptags()
